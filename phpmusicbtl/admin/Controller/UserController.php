<?php
// require("../connect.php");
class UserController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

    public function login()
    {
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $this->model->login($username, $password);
        }
    }
}