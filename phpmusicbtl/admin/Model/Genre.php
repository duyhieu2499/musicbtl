<?php
class Genre{
    private $conn;
    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }

    public function getAll(){
        $stmt = $this->conn->prepare("SELECT * FROM genres");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        print_r($result);
        return $result;
    }
    public function addGenre($name) {
        $stmt = $this->conn->prepare("INSERT INTO genres (name) VALUES (:name)");
        $stmt->bindParam(":name", $name);
        $stmt->execute();
        $genre_id = $this->conn->lastInsertId();
        return $genre_id;
    }

}
