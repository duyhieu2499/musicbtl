<?php
class User{
    private $conn;
    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }

    public function login($username, $password)
    {
        $sql = "SELECT * FROM users where username = :username AND password = :password";
        // var_dump($sql);
        $query = $this->conn->prepare($sql);
        $query->bindParam(':username', $username);
        $query->bindParam(':password', $password);
        $query->execute();

        if ($query->rowCount() == 1) {
            header('location: index.php');
        } else {
            header('location: test.php');
        }
    }

}
